from .forms import *
from .filters import *
from django.views.generic import View
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
def main_view(request):
    return render(request, 'main/base.html', {})

def home_page(request):
    if request.user.is_authenticated:
        user = request.user
        categories = Category.objects.filter(user=user)
        spendings = Spending.objects.filter(category__in = categories)
        return render(request, 'main/spendings_list.html', {'spendings':spendings})
    else:
        return redirect('/accounts/login/')



def spendings_list(request):
    spendings = Spending.objects.all()
    return render(request, 'main/spendings_list.html', {'spendings': spendings})

def spending_new(request):
    if request.method == "POST":
        form = SpendingFormNew(request.user, request.POST)
        if form.is_valid():
            spending = form.save(commit=False)
            spending.user = request.user

            #czy jest to przychód/wydatek wybieramy poprzez listę nie znak w polu cost
            if spending.cost < 0:
                spending.cost *= -1
            if not(spending.oppositeSign):
                spending.cost *= -1

            spending.save()
            return redirect('home_page')
    else:
        form = SpendingFormNew(request.user)
        return render(request, 'main/spending_edit.html', {'form': form})

def spending_edit(request, pk):
    spending = get_object_or_404(Spending, pk=pk)
    if request.method == "POST":
        form = SpendingFormEdit(request.user, request.POST, instance=spending)
        if form.is_valid():
            spending = form.save(commit=False)
            spending.user = request.user

            #czy jest to przychód/wydatek wybieramy poprzez listę nie znak w polu cost
            if spending.cost < 0:
                spending.cost *= -1
            if not(spending.oppositeSign):
                spending.cost *= -1

            spending.save()
            return redirect('home_page')
    else:
        form = SpendingFormEdit(request.user, instance=spending)
        return render(request, 'main/spending_edit.html', {'form': form})

def spending_remove(request, pk):
    spending = get_object_or_404(Spending, pk=pk)
    spending.delete()
    return redirect('home_page')



def categories_list(request):
    if request.user.is_authenticated:
        user = request.user
        categories = Category.objects.filter(user=user)
        return render(request, 'main/categories_list.html', {'categories':categories})
    else:
        return render(request, 'main/categories_list.html')

def category_new(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.user = request.user
            category.save()
            return redirect('home_page')
    else:
        form = CategoryForm()
        return render(request, 'main/category_edit.html', {'form': form})

def category_edit(request, pk):
    category = get_object_or_404(Category, pk=pk)
    if request.method == "POST":
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            category = form.save(commit=False)
            category.user = request.user
            category.save()
            return redirect('categories_list')
    else:
        form = CategoryForm(instance=category)
    return render(request, 'main/category_edit.html', {'form': form})

def category_remove(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.delete()
    return redirect('categories_list')



def search(request):
    category_list = Category.objects.filter(user = request.user)
    spending_list =  Spending.objects.filter(category__in = category_list)
    spending_filter = UserFilter(request.user, request.GET, queryset=spending_list)
    return render(request, 'main/filter.html', {'filter': spending_filter})

#wlasny widok rejestracji
def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)

            # tworzenie domyslnych kategorii
            category = Category(user = user, categoryName = "jedzenie")
            category.save()
            category = Category(user = user, categoryName = "rozrywka")
            category.save()

            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'registration/registration.html', {'form': form})




#-