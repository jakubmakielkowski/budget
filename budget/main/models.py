from django.db import models
from django.conf import settings

class Category(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    categoryName = models.CharField(max_length=200)
    def __str__(self):
        return self.categoryName



BOOL_CHOICES = ((True, 'Przychód'), (False, 'Wydatek'))

class Spending(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    date = models.DateField('Data')
    cost = models.DecimalField(max_digits=10,decimal_places=2)
    # cost = models.DecimalField(_(u'Price'), decimal_places=2, max_digits=10, validators=[MinValueValidator(Decimal('0.01'))])
    oppositeSign = models.BooleanField(blank=False, choices=BOOL_CHOICES)
    comment = models.TextField(blank=True)

    def get_by_owner(cls, request):
        query = request.GET.get("q")
        if query:
            return cls.objects.filter(user=query)
        elif request.user.is_authenticated():
            return cls.objects.all()

    def __str__(self):
        return str(self.cost)
