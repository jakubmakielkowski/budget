from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home_page, name='home_page'),

    url(r'^spendings_list/$', views.home_page, name='home_page'),
    url(r'^spending_new/$', views.spending_new, name='spending_new'),
    url(r'^spending_remove/(?P<pk>\d+)/$', views.spending_remove, name='spending_remove'),
    url(r'^spending_edit/(?P<pk>\d+)/$', views.spending_edit, name='spending_edit'),

    url(r'^categories_list/$', views.categories_list, name='categories_list'),
    url(r'^category_new/$', views.category_new, name='category_new'),
    url(r'^category_remove/(?P<pk>\d+)/$', views.category_remove, name='category_remove'),
    url(r'^category_edit/(?P<pk>\d+)/$', views.category_edit, name='category_edit'),

    url(r'^search/$', views.search, name='search'),
]

