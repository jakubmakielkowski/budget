import django_filters
from django import forms
from .models import *

class DateInput(forms.DateInput):
    input_type = 'date'

class RadioInput(forms.RadioSelect):
    input_type = 'radio'

class UserFilter(django_filters.FilterSet):
    date__gte = django_filters.DateFilter(name='date', lookup_expr='gte', label='Data >= od:', widget=DateInput)
    date__lte = django_filters.DateFilter(name='date', lookup_expr='lte', label='Data <= od:', widget=DateInput)
    date = django_filters.DateFilter(name='date', label='Data:', widget=DateInput)
    cost__gte = django_filters.NumberFilter(name='cost',lookup_expr='gte', label='Wartosc >= od:')
    cost__lte = django_filters.NumberFilter(name='cost',lookup_expr='gte', label='Wartosc >= od:')
    cost = django_filters.NumberFilter(name='cost',lookup_expr='gte', label='Wartosc równa')

    class Meta:
        model = Spending
        fields = ('category', 'date', 'cost','comment','oppositeSign')
        widgets = {
            'oppositeSign': forms.RadioSelect()
        }

    def __init__(self, user, *args, **kwargs):
        super(UserFilter, self).__init__(*args, **kwargs)
        self.filters['category'].field.queryset = Category.objects.filter(user=user)
