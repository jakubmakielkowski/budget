var listitems = document.getElementsByClassName('list-item');
var dates = document.getElementsByClassName('date');
var comments = document.getElementsByClassName('comment');


let costs = [], labels = [], colors = [];
for(let i=0; i<listitems.length; i++){
    costs[i] = listitems[i].children[1].innerHTML;
    labels[i] = dates[i].innerHTML;
    colors[i] = parseFloat(costs[i]) > 0 ? "#83ed8f" : "#ff5c5c";
}
//console.log(parseFloat(costs[0]));

try {
    var ctx = document.getElementById("myChart").getContext('2d');
    (function myChart(){
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'przychód/wydatek',
                    data: costs,
                    backgroundColor: colors
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true

                        }
                    }]
                }
            }
        });
    })();

    function refreshGraph(){
        myChart();  
    }
} catch(e) {

}

function generatePDF(){
    var doc = new jsPDF();
    var pdfYOffset = 25;
    doc.setFillColor(80, 136, 224);
    doc.rect(0, 0, doc.internal.pageSize.width, 14,"F");
    doc.setFontType("normal");
    doc.setTextColor("#ffffff");
    doc.setFontSize(20);
    doc.text(10,10,"Twój raport");

    for(let i=0; i<costs.length; i++){
        doc.setFontType("normal");
        doc.setTextColor("#333333");
        doc.setFontSize(12);
        doc.text(15, pdfYOffset+20*i, labels[i]);

        doc.setTextColor("#666666");
        doc.text(50, pdfYOffset+10+20*i, comments[i].innerHTML);

        doc.setFontType("bold");
        doc.setTextColor(colors[i]);
        doc.setFontSize(16);
        doc.text(15, pdfYOffset+10+20*i, costs[i]);
        doc.setDrawColor("#cccccc");
    }
    doc.output("dataurlnewwindow");
}


try {
    let sum = 0;
    for(let i=0; i<listitems.length; i++){
        sum += parseFloat(costs[i]);
    }
    document.getElementById("sum").innerHTML = "Stan: "+sum.toFixed(2)+" zł";
} catch(e){
    
}



//podczas dodawania wydatków automatycznie zaznacza sie czy to przychód czy wydatek
if(typeof document.getElementById('id_oppositeSign_1') !== 'undefined' && document.getElementById('id_oppositeSign_1') !== null) {
    document.getElementById('id_oppositeSign_1').checked = true;
}


//kolorowanie wydatków i przychodów w spending list
let spendings = document.getElementsByClassName('cost');
for(let i=0; i<spendings.length; i++){
    let val = parseFloat(spendings.item(i).innerHTML);
    if(val >= 0){
        spendings.item(i).style.color = '#408b40';
    } else {
        spendings.item(i).style.color = '#ff5252';
    }
}


//chowanie opcji null podczas wyboru kategorii
document.getElementById('category-field').getElementsByTagName('option')[0].style.display = 'none';
document.getElementById('category-field').getElementsByTagName('option')[1].selected = 'selected';


//blokowanie możliwości usunięcia wszystkich kateogrii
console.log(document.getElementById('category-field').getElementsByTagName('option').length-1);



