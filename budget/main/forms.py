from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from captcha.fields import CaptchaField


class DateInput(forms.DateInput):
    input_type = 'date'

class SpendingFormNew(forms.ModelForm):
    class Meta:
        model = Spending
        fields = ('category', 'date', 'oppositeSign', 'cost', 'comment')
        widgets = {
            'date': DateInput(),
            'oppositeSign': forms.RadioSelect()
        }
    def __init__(self, user, *args, **kwargs):
        super(SpendingFormNew, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(user=user)
        self.fields['category'].widget.attrs.update({'class': 'form-field category-field'})
        self.fields['date'].widget.attrs.update({'class': 'form-field date-field'})
        self.fields['cost'].widget.attrs.update({'class': 'form-field cost-field'})
        self.fields['comment'].widget.attrs.update({'class': 'form-field comment-field'})

class SpendingFormEdit(forms.ModelForm):
    class Meta:
        model = Spending
        fields = ('category', 'date', 'oppositeSign', 'cost', 'comment',)
        widgets = {
            'date': DateInput(),
            'oppositeSign': forms.RadioSelect()
        }
    def __init__(self, user, *args, **kwargs):
        super(SpendingFormEdit, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(user=user)
        self.fields['category'].widget.attrs.update({'class': 'form-field category-field'})
        self.fields['date'].widget.attrs.update({'class': 'form-field date-field'})
        self.fields['cost'].widget.attrs.update({'class': 'form-field cost-field'})
        self.fields['comment'].widget.attrs.update({'class': 'form-field comment-field'})


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('categoryName',)
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['categoryName'].widget.attrs.update({'class': 'form-field category-field'})

class SignUpForm(UserCreationForm):
    captcha = CaptchaField()
    class Meta:
        model = User
        fields = ('username', 'password1', 'password2',)