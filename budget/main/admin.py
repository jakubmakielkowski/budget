from django.contrib import admin
from .models import Category, Spending

admin.site.register(Category)
admin.site.register(Spending)